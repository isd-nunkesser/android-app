package de.hshl.isd.isdcompanion.mensa

import de.hshl.isd.basiccleanarch.Response
import java.util.*

class ConcreteMealGateway : MealGateway {
    override suspend fun fetchMeals(): Response {
        return Response.Success<List<MealEntity>>(
                listOf(
                        MealEntity("Rindergulasch mit Paprika und Zwiebeln",
                                "Hungarian goulash with paprika and onion",
                                Date(), 3.5, 4.0, 6.0,
                                MealEntityCategory.DISH.key,
                                "", HashSet<String>()
                        ),
                        MealEntity("Kartoffelauflauf mit Gemüse und Käse",
                                "Potato gratin with cheese and vegetable",
                                Date(), 3.5, 4.0, 6.5,
                                MealEntityCategory.DISH.key,
                                "", HashSet<String>()),
                        MealEntity("Karamellpudding", "Caramel pudding", Date(),
                                1.2, 1.45, 1.8,
                                MealEntityCategory.DESSERT_COUNTER.key,
                                "", HashSet<String>()),
                        MealEntity("Karamellpudding", "Caramel pudding", Date(),
                                1.2, 1.45, 1.8,
                                MealEntityCategory.DESSERT_COUNTER.key,
                                "", HashSet<String>()),
                        MealEntity("Zucchinigemüse", "Zucchini", Date(), 0.6,
                                0.8, 0.95, MealEntityCategory.SIDEDISH.key,
                                "", HashSet<String>())
                )
        )
    }
}