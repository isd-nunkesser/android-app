package de.hshl.isd.isdcompanion.schedule

data class ScheduleEntryEntity(val dayOfWeek: Int,
                               val startTime: String,
                               val endTime: String,
                               val timeComment: String,
                               val name: String,
                               val lecturer: String,
                               val room: String) {
    val key = """$dayOfWeek$startTime$lecturer$timeComment"""
}
