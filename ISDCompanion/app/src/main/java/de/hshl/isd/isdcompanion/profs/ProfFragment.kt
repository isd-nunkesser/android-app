package de.hshl.isd.isdcompanion.profs

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.basiccleanarch.Response
import de.hshl.isd.basiccleanarch.UseCase
import de.hshl.isd.isdcompanion.R
import kotlinx.android.synthetic.main.fragment_prof_list.*

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [ProfFragment.OnListFragmentInteractionListener] interface.
 */
class ProfFragment : Fragment(), Displayer {

    private lateinit var viewModel: ProfListViewModel
    private var adapter: ListAdapter<ProfViewModel, ProfListAdapter.ViewHolder>? = null
    private var listener: OnListFragmentInteractionListener? = null
    private var interactor: UseCase<Void?, ProfEntity, ProfViewModel>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_prof_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = activity?.run {
            ViewModelProviders.of(this).get(ProfListViewModel::class.java)
        } ?: throw Exception("Invalid Activity")


        viewModel.data.observe(this, Observer { adapter!!.submitList(it) })

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter

        searchView.setOnQueryTextListener(object :
                SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean = false

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText.isNullOrBlank()) {
                    adapter!!.submitList(viewModel.data.value)
                    return false
                }
                val filteredList = viewModel.data.value?.filter {
                    it.name.contains(newText, true) || it.field.contains(newText, true)
                }
                adapter!!.submitList(filteredList)
                return false
            }
        })

        interactor = ProfInteractor(presenter = ProfPresenter(),
                gateway = ProfFromJSONStreamGateway(context!!.assets.open("profs.json")))
        (interactor as ProfInteractor).execute(null,this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
            adapter = ProfListAdapter(listener)
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun display(result: Response) {
        when (result) {
            is Response.Success<*> -> {
                viewModel.submitData(result.value as List<ProfViewModel>)
            }
            is Response.Failure -> {
                Log.i(tag, result.error.localizedMessage)
            }
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: ProfViewModel?)
    }

}
