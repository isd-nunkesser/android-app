package de.hshl.isd.isdcompanion.common


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import de.hshl.isd.isdcompanion.R
import kotlinx.android.synthetic.main.fragment_simple_web_view.*
import java.net.URL

const val ARG_URL = "url"

class SimpleWebViewFragment : Fragment() {
    private var url: URL? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            url = it.getSerializable(ARG_URL) as URL?
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_simple_web_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webView.loadUrl(url.toString())
    }

}
