package de.hshl.isd.isdcompanion.common

interface StorageGateway<T> {
    fun create(obj: T): String
    fun create(key: String, obj: T)
    fun retrieve(key: String): T?
    fun retrieveAll(): Collection<T>
    fun update(key: String, obj: T): Boolean
    fun delete(key: String): Boolean
}