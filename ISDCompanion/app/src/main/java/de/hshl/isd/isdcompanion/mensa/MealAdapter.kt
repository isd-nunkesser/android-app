package de.hshl.isd.isdcompanion.mensa


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import de.hshl.isd.isdcompanion.R
import de.hshl.isd.isdcompanion.common.ItemViewModel
import kotlinx.android.synthetic.main.fragment_mensa.view.*

class MealAdapter : ListAdapter<ItemViewModel, MealAdapter.ItemViewHolder>(
        object : DiffUtil.ItemCallback<ItemViewModel>() {
            override fun areItemsTheSame(oldItem: ItemViewModel, newItem: ItemViewModel): Boolean = oldItem == newItem

            override fun areContentsTheSame(oldItem: ItemViewModel, newItem: ItemViewModel): Boolean = oldItem.content.equals(newItem.content)

        }
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        when (viewType) {
            ItemViewModel::class.hashCode() -> return ItemViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_item_header, parent, false))
            else -> return MealViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.fragment_mensa, parent, false))
        }
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = getItem(position)
        val content = item.content
        holder.mContentView.text = content
        if (holder is MealViewHolder && item is MealViewModel) {
            holder.mPricesView.text = item.price
            if (item.image != null) {
                Picasso.get().load(item.image.toString())
                        .resize(200, 200)
                        .centerCrop()
                        .into(holder.mImageView)
            }
        }
    }

    override fun getItemViewType(position: Int): Int = getItem(position)::class.hashCode()

    open inner class ItemViewHolder(mView: View) : RecyclerView.ViewHolder(mView) {
        val mContentView: TextView = mView.content
    }

    inner class MealViewHolder(val mView: View) : ItemViewHolder(mView) {
        val mImageView: ImageView = mView.meal_image
        val mPriceLabelView: TextView = mView.label_mensa_prices
        val mPricesView: TextView = mView.meal_prices
    }
}
