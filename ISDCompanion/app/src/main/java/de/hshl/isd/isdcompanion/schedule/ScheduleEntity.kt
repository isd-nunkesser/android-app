package de.hshl.isd.isdcompanion.schedule

data class ScheduleEntity(val term: Int,
                          val group: String,
                          val fieldOfStudy: String,
                          val entries: List<ScheduleEntryEntity>)
