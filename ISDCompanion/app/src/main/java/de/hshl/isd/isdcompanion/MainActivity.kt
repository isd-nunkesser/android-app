package de.hshl.isd.isdcompanion

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity
import de.hshl.isd.isdcompanion.common.ARG_URL
import de.hshl.isd.isdcompanion.profs.ProfFragment
import de.hshl.isd.isdcompanion.profs.ProfViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), ProfFragment.OnListFragmentInteractionListener {
    private var navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        navController = findNavController(R.id.nav_host_fragment)
        setupActionBarWithNavController(navController!!)
        navigation.setupWithNavController(navController!!)
    }

    override fun onSupportNavigateUp() = navController!!.navigateUp()

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.action_info -> {
                startActivity(Intent(this, OssLicensesMenuActivity::class.java))
                return false
            }
            else -> {
            }
        }

        return false
    }

    override fun onListFragmentInteraction(item: ProfViewModel?) {
        val bundle = Bundle()
        bundle.putSerializable(ARG_URL, item!!.link)
        navController?.navigate(
                R.id.action_profFragment_to_simpleWebViewFragment,
                bundle)
    }

}
