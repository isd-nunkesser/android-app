package de.hshl.isd.isdcompanion.profs

import de.hshl.isd.basiccleanarch.Presenter

class ProfPresenter : Presenter<ProfEntity,ProfViewModel> {
    override fun present(model: ProfEntity): ProfViewModel {
        return ProfViewModel(model.name,model.field,model.phone,model.mail,model.link)
    }
}