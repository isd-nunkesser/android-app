package de.hshl.isd.isdcompanion.schedule

import android.content.Intent
import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.basiccleanarch.Presenter
import de.hshl.isd.basiccleanarch.Response
import de.hshl.isd.basiccleanarch.UseCase

class ScheduleEntryExportInteractor(override val presenter: Presenter<ScheduleEntryEntity, Intent>) : UseCase<ScheduleEntryExportRequest, ScheduleEntryEntity, Intent> {
    override fun execute(request: ScheduleEntryExportRequest, displayer: Displayer) {
        val entry = ScheduleStorageObject.retrieve(request.key)
        displayer.display(Response.Success(presenter.present(entry!!)))
    }
}