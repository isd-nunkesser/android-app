package de.hshl.isd.isdcompanion.mensa

enum class MealEntityCategory(val key: String, var order: Int) {
    DISH("dish", 0), SIDEDISH("sidedish", 1), DESSERT("dessert", 3),
    SOUPS("soups", 2),
    DISH_DEFAULT("dish-default", 0), DESSERT_COUNTER("dessert-counter", 3),
    DISH_GRILL("dish-grill", 0), UNKNOWN("-", 4);
}