package de.hshl.isd.isdcompanion.mensa

import de.hshl.isd.basiccleanarch.Response

interface MealGateway {
    suspend fun fetchMeals(): Response
}