package de.hshl.isd.isdcompanion.mensa

import de.hshl.isd.isdcompanion.common.ItemViewModel
import java.net.URL

class MealViewModel(content: String, val price: String, val category: MealViewModelCategory, val image: URL?,
                    val allergens: Set<String>) : ItemViewModel(content), Comparable<MealViewModel> {
    override fun compareTo(other: MealViewModel): Int {
        if (category.equals(other.category)) return content.compareTo(other.content)
        return (category.ordinal).compareTo(other.category.ordinal)
    }
}