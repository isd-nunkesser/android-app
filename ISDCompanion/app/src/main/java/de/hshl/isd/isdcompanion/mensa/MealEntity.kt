package de.hshl.isd.isdcompanion.mensa

import java.net.URL
import java.util.*

data class MealEntity(val name_de: String, val name_en: String, val date: Date, val priceStudents: Double, val priceWorkers: Double, val priceGuests: Double,
                      val category: String, val image: String, val allergens: Set<String>)

