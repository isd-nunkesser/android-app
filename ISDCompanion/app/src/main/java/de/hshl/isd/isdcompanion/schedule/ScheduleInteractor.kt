package de.hshl.isd.isdcompanion.schedule

import com.github.eunsiljo.timetablelib.data.TimeData
import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.basiccleanarch.Presenter
import de.hshl.isd.basiccleanarch.Response
import de.hshl.isd.basiccleanarch.UseCase
import java.util.*

class ScheduleInteractor(
        override val presenter: Presenter<ScheduleEntryEntity, TimeData<String>>,
        val gateway: ScheduleFromJSONStreamGateway) : UseCase<ScheduleRequest, ScheduleEntryEntity, TimeData<String>> {

    override fun execute(request: ScheduleRequest, displayer: Displayer) {
        // Fetch schedule entries for term
        val result = gateway.fetch(request.index)

        // Persist entries with generated keys
        for (entry in result) {
            ScheduleStorageObject.create(entry.key, entry)
        }

        val weekSchedule = result.groupBy { it.dayOfWeek }
        val tables = arrayOfNulls<ArrayList<TimeData<Any>>>(5)
        for (day in weekSchedule.keys) {
            val dayViewModels = weekSchedule[day]!!.map { presenter.present(it) }
            val dayArrayList = ArrayList<TimeData<String>>()
            for (dayViewModel in dayViewModels) {
                dayArrayList.add(dayViewModel)
            }
            tables[day - 1] = dayArrayList as ArrayList<TimeData<Any>>
        }

        displayer.display(Response.Success(tables))
    }
}