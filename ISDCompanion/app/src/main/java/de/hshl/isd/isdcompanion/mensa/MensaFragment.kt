package de.hshl.isd.isdcompanion.mensa

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.preference.PreferenceManager
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.basiccleanarch.Response
import de.hshl.isd.basiccleanarch.UseCase
import de.hshl.isd.isdcompanion.R
import de.hshl.isd.isdcompanion.common.ErrorHandler
import de.hshl.isd.isdcompanion.common.ErrorHandler.Companion.ERROR_ALERT
import de.hshl.isd.isdcompanion.common.ErrorHandler.Companion.PROGRESS_END
import de.hshl.isd.isdcompanion.common.ItemViewModel
import kotlinx.android.synthetic.main.fragment_mensa_list.*
import java.util.*


/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [MensaFragment.OnListFragmentInteractionListener] interface.
 */
class MensaFragment : Fragment(), Displayer {
    private val interactor: UseCase<MealRequest, MealEntity, MealViewModel> = MealInteractor(MealPresenter())
    private val filter = HashSet<String>()
    private var handler: Handler? = null
    private var columnCount = 1
    private lateinit var viewModel : MealsViewModel
    private val adapter = MealAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }

        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.mensa, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.action_mensa_info -> {
                val builder = AlertDialog.Builder(activity)
                builder.setMessage(R.string.mensa_info)
                        .setTitle(R.string.info)
                        .setPositiveButton(android.R.string.ok, null)
                        .setNeutralButton(R.string.show, DialogInterface.OnClickListener { dialogInterface, i ->
                            val url = getString(R.string.mensa_infopage)
                            val i = Intent(Intent.ACTION_VIEW)
                            i.data = Uri.parse(url)
                            startActivity(i)
                        })
                val dialog = builder.create()
                dialog.show()
                return false
            }
            else -> {
            }
        }

        return false
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mensa_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(MealsViewModel::class.java)
        viewModel.data.observe(this, Observer { adapter.submitList(it) })

        list.layoutManager = when {
            columnCount <= 1 -> LinearLayoutManager(context)
            else -> GridLayoutManager(context, columnCount)
        }
        list.adapter = adapter

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)

        for (i in 1..15) {
            if (sharedPref.getBoolean("allergen_a$i", true)) {
                filter.remove("A$i")
            } else {
                filter.add("A$i")
            }
            if (sharedPref.getBoolean("addition_$i", true)) {
                filter.remove(i.toString())
            } else {
                filter.add(i.toString())
            }
        }

        handler = ErrorHandler.newInstance(Looper.getMainLooper(), activity!!)

        progressBar.visibility = View.VISIBLE
        interactor.execute(MealRequest(filter), displayer = this)
    }

    override fun display(result: Response) {
        val progressEndMessage = handler?.obtainMessage(PROGRESS_END, progressBar)
        progressEndMessage?.sendToTarget()
        when (result) {
            is Response.Success<*> -> {
                val viewModels = (result as Response.Success<List<ItemViewModel>>).value
                viewModel.update(viewModels)
            }
            is Response.Failure -> {
                var title = R.string.error_dialog_title
                var content = R.string.error_dialog_message
                if (result.error is NoMealsException) {
                    title = R.string.info
                    content = R.string.error_no_meals
                }
                val message = handler?.obtainMessage(ERROR_ALERT,
                        title,
                        content)
                message?.sendToTarget()
            }
        }
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
                MensaFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_COLUMN_COUNT, columnCount)
                    }
                }
    }
}
