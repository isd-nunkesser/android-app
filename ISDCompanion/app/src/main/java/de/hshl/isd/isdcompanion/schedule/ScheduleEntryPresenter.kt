package de.hshl.isd.isdcompanion.schedule

import com.github.eunsiljo.timetablelib.data.TimeData
import de.hshl.isd.basiccleanarch.Presenter
import de.hshl.isd.isdcompanion.R
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

class ScheduleEntryPresenter : Presenter<ScheduleEntryEntity, TimeData<String>> {
    override fun present(model: ScheduleEntryEntity): TimeData<String> {
        colorSwitch = !colorSwitch
        var color = R.color.color_table_1_light
        if (colorSwitch) {
            color = R.color.color_table_2_light
        }
        val startTime = getMillis("2019-03-01 ${model.startTime}")
        val endTime = getMillis("2019-03-01 ${model.endTime}")

        var text = "${model.name}\n${model.lecturer}\n${model.room}"
        if (model.timeComment.isNotEmpty()) {
            text = "${model.timeComment}\n${text}"
        }

        return TimeData(model.key, text, color, startTime, endTime)
    }

    private fun getMillis(day: String): Long {
        val date = getDateTimePattern().parseDateTime(day)
        return date.millis
    }

    private fun getDateTimePattern(): DateTimeFormatter {
        return DateTimeFormat.forPattern("yyyy-MM-dd HH:mm")
    }

    companion object {
        private var colorSwitch = true
    }
}