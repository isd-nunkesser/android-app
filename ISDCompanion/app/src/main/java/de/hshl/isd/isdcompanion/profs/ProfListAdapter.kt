package de.hshl.isd.isdcompanion.profs

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import de.hshl.isd.isdcompanion.R
import kotlinx.android.synthetic.main.fragment_prof.view.*

class ProfListAdapter(private val mListener: ProfFragment.OnListFragmentInteractionListener?) : ListAdapter<ProfViewModel, ProfListAdapter.ViewHolder>(
        object : DiffUtil.ItemCallback<ProfViewModel>() {
            override fun areItemsTheSame(oldItem: ProfViewModel, newItem: ProfViewModel):
                    Boolean = oldItem == newItem

            override fun areContentsTheSame(oldItem: ProfViewModel, newItem: ProfViewModel):
                    Boolean = oldItem.equals(newItem)
        }) {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as ProfViewModel
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_prof, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.mTextView.text = item.name
        holder.mDetailView.text = item.field

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mTextView: TextView = mView.text
        val mDetailView: TextView = mView.detail
    }
}