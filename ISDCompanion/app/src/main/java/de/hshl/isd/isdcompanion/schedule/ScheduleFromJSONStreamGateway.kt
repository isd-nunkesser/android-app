package de.hshl.isd.isdcompanion.schedule

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.io.InputStream

/**
 * Created by nunkesser on 28.02.18.
 */

class ScheduleFromJSONStreamGateway(val input: InputStream) {

    fun fetch(term: Int): Collection<ScheduleEntryEntity> {
        val schedules = jacksonObjectMapper().readValue<List<ScheduleEntity>>(input)
        return schedules.elementAt(term).entries
    }
}
