package de.hshl.isd.isdcompanion.semesterdates


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.hshl.isd.isdcompanion.R
import de.hshl.isd.isdcompanion.common.ItemViewModel
import kotlinx.android.synthetic.main.fragment_semesterdate.view.*

class MySemesterDateRecyclerViewAdapter(
        private val mValues: List<ItemViewModel>)
    : RecyclerView.Adapter<MySemesterDateRecyclerViewAdapter.ItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ItemViewHolder {
        when (viewType) {
            ItemViewModel::class.hashCode() -> return ItemViewHolder(
                    LayoutInflater.from(parent.context)
                            .inflate(R.layout.list_item_header, parent, false))
            else -> return DateViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.fragment_semesterdate, parent, false))
        }
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = mValues[position]
        holder.mContentView.text = item.content

        if (holder is DateViewHolder && item is SemesterDateViewModel) {
            holder.mIdView.text = item.id
            with(holder.mView) {
                tag = item
            }
        }
    }

    override fun getItemCount(): Int = mValues.size

    override fun getItemViewType(position: Int): Int =
            mValues[position]::class.hashCode()


    inner class DateViewHolder(val mView: View) : ItemViewHolder(mView) {
        val mIdView: TextView = mView.item_number
    }

    open inner class ItemViewHolder(mView: View) :
            RecyclerView.ViewHolder(mView) {
        val mContentView: TextView = mView.content
    }

}
