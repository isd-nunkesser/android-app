package de.hshl.isd.isdcompanion.mensa

import de.hshl.isd.basiccleanarch.Presenter
import java.net.MalformedURLException
import java.net.URL
import java.text.NumberFormat
import java.util.*

class MealPresenter : Presenter<MealEntity,MealViewModel> {
    override fun present(model: MealEntity): MealViewModel {
        val language = Locale.getDefault().language
        var name = model.name_en
        if (language.equals("de")) {
            name = model.name_de
        }
        val formatter = NumberFormat.getCurrencyInstance(Locale.GERMANY)
        val price = formatter.format(model.priceStudents) + " / " +
                formatter.format(
                        model.priceWorkers) + " / " +
                formatter.format(model.priceGuests)
        var category = MealViewModelCategory.DISH
        when (model.category) {
            "dish", "dish-default", "dish-grill" -> category = MealViewModelCategory.DISH
            "sidedish" -> category = MealViewModelCategory.SIDEDISH
            "dessert", "dessert-counter" -> category = MealViewModelCategory.DESSERT
            "soups" -> category = MealViewModelCategory.SOUPS
        }
        var url : URL? = null
        try {
            url = URL(model.image)
        } catch (e: MalformedURLException) {
            // Intentionally do nothing
        }

        return MealViewModel(name, price, category,
                url, model.allergens)
    }

}