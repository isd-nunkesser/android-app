package de.hshl.isd.isdcompanion.profs

import java.net.URL

data class ProfViewModel(val name: String, val field: String, val phone: String, val mail: String,
                val link: URL)
