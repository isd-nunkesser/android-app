package de.hshl.isd.isdcompanion.semesterdates

import de.hshl.isd.isdcompanion.common.ItemViewModel

class SemesterDateViewModel(val id: String, content: String,
                            val details: String) : ItemViewModel(content) {
    override fun toString(): String = content
}