package de.hshl.isd.isdcompanion.mensa

import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.basiccleanarch.Presenter
import de.hshl.isd.basiccleanarch.Response
import de.hshl.isd.basiccleanarch.UseCase
import de.hshl.isd.isdcompanion.MainApplication
import de.hshl.isd.isdcompanion.common.ItemViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class MealInteractor(override val presenter: Presenter<MealEntity, MealViewModel>) : UseCase<MealRequest, MealEntity, MealViewModel> {
    override fun execute(request: MealRequest, displayer: Displayer) {
        GlobalScope.async {

            val response = ConcreteMealGateway().fetchMeals()
            when (response) {
                is Response.Success<*> -> {
                    var mealEntities = (response.value as List<MealEntity>)
                    mealEntities = mealEntities.filter { it.allergens.intersect(request.filter).isEmpty() }
                    val meals = mealEntities.map { MealPresenter().present(it) }.sorted()
                    val groups = meals.groupBy { it.category }
                    val mealList: MutableList<ItemViewModel> = mutableListOf()
                    for (collection in groups.keys) {
                        val stringId = collection.label.toInt()
                        mealList.add(
                                ItemViewModel(
                                        MainApplication.applicationContext().getString(
                                                stringId)))
                        for (meal in groups[collection]!!) {
                            mealList.add(meal)
                        }
                    }
                    if (mealList.size > 0) {
                        displayer.display(Response.Success(mealList))
                    } else {
                        displayer.display(Response.Failure(NoMealsException()))
                    }
                }
                is Response.Failure -> {
                    displayer.display(response)
                }
            }
        }
    }
}