package de.hshl.isd.isdcompanion.semesterdates

import de.hshl.isd.isdcompanion.MainApplication
import de.hshl.isd.isdcompanion.R
import de.hshl.isd.isdcompanion.common.ItemViewModel
import java.util.*

object SemesterDatesStaticContent {

    val ITEMS: MutableList<ItemViewModel> = ArrayList()

    init {
        ITEMS.add(ItemViewModel(MainApplication.applicationContext().getString(
                R.string.summer_semester)))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.semester_date_1), "31.01.2020", ""))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.semester_date_2), "09.03.2020", ""))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.semester_date_3), "20.04.2020", ""))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.semester_date_4), "06.04.2020", ""))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.summer_date_1), "14.04.2020-17.04.2020", ""))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.summer_date_2), "02.06.2020", ""))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.semester_date_5), "22.06.2020", ""))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.semester_date_6), "26.06.2020", ""))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.semester_date_7), "29.06.2020-17.07.2020", ""))

        ITEMS.add(ItemViewModel(MainApplication.applicationContext().getString(
                R.string.winter_semester)))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.semester_date_1), "31.07.2020", ""))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.semester_date_2), "14.09.2020", ""))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.semester_date_3), "28.09.2020", ""))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.semester_date_4), "12.10.2020", ""))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.winter_date_1), "23.12.2020-01.01.2021", ""))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.semester_date_5), "18.01.2021", ""))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.semester_date_6), "22.01.2021", ""))
        ITEMS.add(SemesterDateViewModel(
                MainApplication.applicationContext().getString(
                        R.string.semester_date_7), "25.01.2021-12.02.2021", ""))
    }


}
