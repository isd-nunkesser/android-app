package de.hshl.isd.isdcompanion.profs

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import de.hshl.isd.isdcompanion.profs.ProfEntity

import java.io.InputStream

/**
 * Created by nunkesser on 28.02.18.
 */

class ProfFromJSONStreamGateway(val input: InputStream) {

    fun fetch(): Collection<ProfEntity> {
        return jacksonObjectMapper().readValue<List<ProfEntity>>(input)
    }
}
