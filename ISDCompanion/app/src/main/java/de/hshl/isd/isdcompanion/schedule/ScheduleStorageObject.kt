package de.hshl.isd.isdcompanion.schedule

import android.os.Build
import de.hshl.isd.isdcompanion.common.StorageGateway
import java.util.*

object ScheduleStorageObject : StorageGateway<ScheduleEntryEntity> {
    private val entities = Hashtable<String, ScheduleEntryEntity>()
    private var autoKey = 0

    override fun create(obj: ScheduleEntryEntity): String {
        entities[autoKey.toString()] = obj
        return (autoKey++).toString()
    }

    override fun create(key: String, obj: ScheduleEntryEntity) {
        entities[key] = obj
    }

    override fun retrieve(key: String): ScheduleEntryEntity? {
        return entities[key]
    }

    override fun retrieveAll(): Collection<ScheduleEntryEntity> {
        return entities.values
    }

    override fun update(key: String, obj: ScheduleEntryEntity): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return entities.replace(key, entities[key], obj)
        } else {
            entities[key] = obj
            return true
        }
    }

    override fun delete(key: String): Boolean {
        return (entities.remove(key) != null)
    }
}