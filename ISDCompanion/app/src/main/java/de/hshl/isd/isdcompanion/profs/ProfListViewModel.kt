package de.hshl.isd.isdcompanion.profs

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ProfListViewModel  : ViewModel() {

        private val _data = MutableLiveData<List<ProfViewModel>>()

        val data: LiveData<List<ProfViewModel>>
            get() = _data

        init {
            _data.value = listOf()
        }

        fun submitData(items : List<ProfViewModel>) {
            _data.postValue(items)
        }
    }
