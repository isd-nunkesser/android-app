package de.hshl.isd.isdcompanion.mensa

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import de.hshl.isd.isdcompanion.common.ItemViewModel

class MealsViewModel : ViewModel() {
    private val _data = MutableLiveData<List<ItemViewModel>>()
    val data: LiveData<List<ItemViewModel>>
        get() = _data

    init {
        _data.value = listOf()
    }

    fun update(values: List<ItemViewModel>) {
        _data.postValue(values)
    }

}