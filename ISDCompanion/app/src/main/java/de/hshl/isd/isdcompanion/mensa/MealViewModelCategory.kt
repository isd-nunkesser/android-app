package de.hshl.isd.isdcompanion.mensa

import de.hshl.isd.isdcompanion.R

enum class MealViewModelCategory(val label: String) {
    DISH(R.string.dishes.toString()), SOUPS(R.string.soups.toString()), SIDEDISH(R.string.sidedishes.toString()),
    DESSERT(R.string.desserts.toString())
}