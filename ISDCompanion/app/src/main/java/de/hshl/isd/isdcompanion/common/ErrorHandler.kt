package de.hshl.isd.isdcompanion.common

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.view.View
import android.widget.ProgressBar
import de.hshl.isd.isdcompanion.R


class ErrorHandler private constructor(looper: Looper) : Handler(looper) {

    private var mActivity: Activity? = null

    override fun handleMessage(msg: Message) {
        super.handleMessage(msg)
        when (msg.what) {
            ERROR_ALERT -> {
                val builder = AlertDialog.Builder(mActivity)
                builder.setMessage(msg.arg2)
                        .setTitle(msg.arg1)
                        .setPositiveButton(android.R.string.ok, null)
                val dialog = builder.create()
                dialog.show()
            }
            ERROR_RETRY -> {
                val builder = AlertDialog.Builder(mActivity)
                builder.setMessage(msg.arg2)
                        .setTitle(msg.arg1)
                        .setPositiveButton(android.R.string.ok, null)
                        .setNeutralButton(R.string.retry, msg.obj as DialogInterface.OnClickListener)
                val dialog = builder.create()
                dialog.show()
            }
            PROGRESS_END -> {
                if (msg.obj is ProgressBar) {
                    (msg.obj as ProgressBar).visibility = View.GONE
                }

            }
        }

    }

    companion object {

        val ERROR_ALERT = 0
        val ERROR_RETRY = 1
        val PROGRESS_END = 2

        fun newInstance(looper: Looper, activity: Activity): ErrorHandler {
            val handler = ErrorHandler(looper)
            handler.mActivity = activity
            return handler
        }
    }
}
