package de.hshl.isd.isdcompanion.profs

import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.basiccleanarch.Presenter
import de.hshl.isd.basiccleanarch.Response
import de.hshl.isd.basiccleanarch.UseCase

class ProfInteractor(
        override val presenter: Presenter<ProfEntity, ProfViewModel>,
        val gateway: ProfFromJSONStreamGateway) : UseCase<Void?,ProfEntity,ProfViewModel> {

    override fun execute(request: Void?, displayer: Displayer) {
        val result = gateway.fetch()
        val viewModel = result.map { presenter.present(it) }
        displayer.display(Response.Success(viewModel))
    }
}