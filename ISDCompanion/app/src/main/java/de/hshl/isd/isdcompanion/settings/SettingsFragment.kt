package de.hshl.isd.isdcompanion.settings


import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import de.hshl.isd.isdcompanion.R


class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.pref_general, rootKey)
    }

}

