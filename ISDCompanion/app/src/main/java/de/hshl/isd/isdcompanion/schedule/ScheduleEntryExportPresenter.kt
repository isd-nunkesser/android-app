package de.hshl.isd.isdcompanion.schedule

import android.content.Intent
import android.provider.CalendarContract
import de.hshl.isd.basiccleanarch.Presenter
import org.joda.time.format.DateTimeFormat
import java.util.*

class ScheduleEntryExportPresenter : Presenter<ScheduleEntryEntity, Intent> {

    val startDate = "2020-04-20"
    val normalRecurrency = "FREQ=WEEKLY;COUNT=14;WKST=SU"
    val oddRecurrency = "FREQ=WEEKLY;INTERVAL=2;COUNT=7;WKST=SU"
    val evenRecurrency = "FREQ=WEEKLY;INTERVAL=2;COUNT=7;WKST=SU"
    val startDateOddWeek = "2020-04-20"
    val startDateEvenWeek = "2020-04-27"
    val dateTimeFomat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm")

    override fun present(model: ScheduleEntryEntity): Intent {
        val calIntent = Intent(Intent.ACTION_INSERT)
        calIntent.type = "vnd.android.cursor.item/event"

        var start = startDate
        var recurrency = normalRecurrency
        if (model.timeComment.equals("uKW")) {
            start = startDateOddWeek
            recurrency = oddRecurrency
        } else if (model.timeComment.equals("gKW")) {
            start = startDateEvenWeek
            recurrency = evenRecurrency
        }
        calIntent.putExtra(CalendarContract.Events.TITLE, model.name)
        calIntent.putExtra(CalendarContract.Events.EVENT_LOCATION, model.room)
        calIntent.putExtra(CalendarContract.Events.DESCRIPTION, model.lecturer)


        val startTime = dateTimeFomat.parseDateTime("${start} ${model.startTime}").toGregorianCalendar()
        val endTime = dateTimeFomat.parseDateTime("${start} ${model.endTime}").toGregorianCalendar()
        startTime.add(GregorianCalendar.DAY_OF_MONTH, model.dayOfWeek - 1)
        endTime.add(GregorianCalendar.DAY_OF_MONTH, model.dayOfWeek - 1)

        calIntent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false)
        calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                startTime.timeInMillis)
        calIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                endTime.timeInMillis)

        calIntent.putExtra(CalendarContract.Events.RRULE, recurrency)
        return calIntent
    }
}

