package de.hshl.isd.isdcompanion.schedule

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import com.github.eunsiljo.timetablelib.data.TimeData
import com.github.eunsiljo.timetablelib.data.TimeGridData
import com.github.eunsiljo.timetablelib.data.TimeTableData
import com.github.eunsiljo.timetablelib.view.TimeTableView
import com.github.eunsiljo.timetablelib.viewholder.TimeTableItemViewHolder
import de.hshl.isd.basiccleanarch.Displayer
import de.hshl.isd.basiccleanarch.Response
import de.hshl.isd.basiccleanarch.UseCase
import de.hshl.isd.isdcompanion.R
import kotlinx.android.synthetic.main.schedule_fragment.*
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList

class ScheduleFragment : Fragment(), Displayer {

    private var interactor: UseCase<ScheduleRequest, ScheduleEntryEntity, TimeData<String>>? = null
    private var tableMode = TimeTableView.TableMode.LONG
    val tables = ArrayList<TimeTableData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.schedule, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.action_schedule_switch -> {
                if (tableMode == TimeTableView.TableMode.LONG) {
                    tableMode = TimeTableView.TableMode.SHORT
                } else {
                    tableMode = TimeTableView.TableMode.LONG
                }
                timeTable.setTableMode(tableMode)
                setTimeTable()
                return false
            }
            else -> {
            }
        }

        return false
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.schedule_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        timeTable.setStartHour(8)
        timeTable.setShowHeader(true)
        timeTable.setTableMode(tableMode)

        timeTable.setOnTimeItemClickListener(object : TimeTableItemViewHolder.OnTimeItemClickListener {
            override fun onTimeItemClick(view: View, position: Int, item: TimeGridData) {
                val builder = AlertDialog.Builder(activity)
                builder.setMessage(R.string.calendar_export)
                        .setTitle(R.string.info)
                        .setPositiveButton(R.string.calendar_export_confirm, DialogInterface.OnClickListener { dialogInterface, i -> addCalendarEntry(item.time) })
                        .setNeutralButton(android.R.string.cancel, null)
                val dialog = builder.create()
                dialog.show()
            }
        })

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)
        val request =
                ScheduleRequest(sharedPref.getString("schedule", "0")!!.toInt())

        interactor = ScheduleInteractor(presenter = ScheduleEntryPresenter(),
                gateway = ScheduleFromJSONStreamGateway(context!!.assets.open("schedule.json")))
        (interactor as ScheduleInteractor).execute(request, this)

    }

    private fun addCalendarEntry(entry: TimeData<Any>) {
        val exportInteractor = ScheduleEntryExportInteractor(ScheduleEntryExportPresenter())
        exportInteractor.execute(ScheduleEntryExportRequest(entry.key as String), this)
    }

    private fun getMillis(day: String): Long {
        val date = getDateTimePattern().parseDateTime(day)
        return date.millis
    }

    private fun getDateTimePattern(): DateTimeFormatter {
        return DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
    }

    override fun display(result: Response) {
        when (result) {
            is Response.Success<*> -> {
                when (result.value) {
                    is Array<*> -> {
                        val array: Array<String> = resources.getStringArray(R.array.weekdays_array)
                        val week = result.value as Array<ArrayList<TimeData<Any>>>
                        for (index in 0..4) {
                            if (week[index] != null) {
                                tables.add(TimeTableData(array[index], week[index]))
                            }
                        }
                        setTimeTable()
                    }
                    is Intent -> {
                        startActivity(result.value as Intent)
                    }
                }

            }
            is Response.Failure -> {
                Log.i(tag, result.error.localizedMessage)
            }
        }
    }

    fun setTimeTable() {
        if (tableMode == TimeTableView.TableMode.LONG) {
            val c = Calendar.getInstance()
            var day = c.get(Calendar.DAY_OF_WEEK)
            val dayTable = java.util.ArrayList<TimeTableData>()
            var index = day - 2
            while (tables.getOrNull(index) == null) {
                index = (index + 1) % 5
            }
            dayTable.add(tables.elementAt(index))
            timeTable.setTimeTable(getMillis("2019-03-01 00:00:00"), dayTable)
        } else {
            timeTable.setTimeTable(getMillis("2019-03-01 00:00:00"), tables)
        }

    }

}
