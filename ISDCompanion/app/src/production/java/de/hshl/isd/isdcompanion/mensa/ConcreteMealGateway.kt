package de.hshl.isd.isdcompanion.mensa

import de.hshl.isd.basiccleanarch.Response
import de.hshl.isd.isdcompanion.MainApplication
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.*


class ConcreteMealGateway : MealGateway {
    private val formatter = SimpleDateFormat("yyyy-MM-dd")

    override suspend fun fetchMeals(): Response {
        val retrofit = Retrofit.Builder().baseUrl("http://www.studentenwerk-pb.de").addConverterFactory(GsonConverterFactory.create()).build()
        val service = retrofit.create(StudentenwerkPaderbornAPI::class.java)

        try {
            val props = Properties()
            props.load(MainApplication.applicationContext().assets
                    .open("mensasecret.properties"))
            val response = service.fetch(props.getProperty("secret"), "mensa-hamm", formatter.format(Date())).execute()
            return Response.Success(response.body()!!)
        } catch (t: Throwable) {
            return Response.Failure(t)
        }
    }
}


