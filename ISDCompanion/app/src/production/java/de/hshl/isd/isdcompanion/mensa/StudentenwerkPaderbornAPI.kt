package de.hshl.isd.isdcompanion.mensa

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface StudentenwerkPaderbornAPI {
    @GET("fileadmin/shareddata/access2.php")
    fun fetch(@Query("id") id : String,
              @Query("restaurant") restaurant : String,
              @Query("date") date : String): Call<List<MealEntity>>

}